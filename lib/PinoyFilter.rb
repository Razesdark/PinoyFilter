require "PinoyFilter/version"
require "PinoyFilter/Classes/Anime"
require "PinoyFilter/Classes/Episode"
require "PinoyFilter/SiteHandlers/SiteHandler"

# Main module that holds all the classes in this gem.
# The function you are looking to interface with is probably PinoyFilter::Site
module PinoyFilter
  # This class serves as the binding class
  # that holds all the central functions of the gem
  # there should be no reason for anyone to interact
  # with the other classes directly, without them being
  # spawned by this class.
  class Site
    # call-seq:
    #   initialize(void) => void
    #   initialize(string) => void
    # The function will declare the domain used in the rest of the functions
    # Should PinoyAnime go down or change name, etc. Hopefully this software will still work if you pass it the new address.
    # must start with be http://, can not start with https://
    def initialize(url="http://www.theanime.tv/")
      @url = url
      if @url[-1,1] != "/"
        @url = "#{@url}/"
      end

      if @url[0, 7] != "http://"
        @url = "http://#{@url}"
      end
    end

    # This function will output the url supplied when .new was called.
    # The url will have been checked and corrected for any mistakes when .new was called in the first place.
    # call-seq:
    #   url => string
    def url
      return @url
    end

    #Custom Inspect message.
    def inspect
      return "#{self}, URL: #{@url}"
    end
    # call-seq:
    #   animes => Pinoy::Anime[]
    # = Workings
    # This function calls http://www.pinoyanime.tv/anime-lists/
    # and parses its HTML to get a list of all animation series currently listed on their webpage
    #
    # = Missing functions
    # The current filter is somewhat lacking and extracts to many links to pages
    # that are not formatted correctly and will break the system if you try to "watch" them.
    #
    # In the future it will be improved to do the following
    # * <b>Check if the name is already in the system.</b> The sidebar uses the same html scheme as the main list
    # so current series will be parsed and listed twice with two ID's.
    # Since the link is the same both entities will work, but are useless. Also they do not appear in order.
    # * <b>Check URL to filter off site links and other non valid ones.</b>
    def animes
      return PinoyFilter::SiteHandler::GetAnimeList(@url)
    end

    # call-seq:
    #   latest => Pinoy::Episode[]
    # = Workings
    # Loads front page and fetches a list over the X most recently updated
    # anime series.
    # It loads a list of episodes, not anime elements, so the anime title is missing
    # for now.
    #
    # This will be updated in the future.
    #
    def latest
      return PinoyFilter::SiteHandler::GetLatest(@url)
    end
  end
end
