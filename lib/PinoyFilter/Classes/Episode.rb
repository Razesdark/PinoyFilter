module PinoyFilter

  # The episode class holds basic data about a episode, like @title and @url
  # It is also able to find the direct stream URL suing nokogiri.
  class Episode
    # Pinoy::EpisodeClass.streamURL
    # call-seq:
    #   streamURL(Nokogiri::element) => void
    # = Workings
    # This function requires a 'a' element from nokogiri
    # usually done throught a page.css('a') call or similar.
    #
    # = Accepts
    # This function accepts two kind of HTML elements. Either links or table-rows.
    # The table rows are used when you parse anime webpage, links are used when parsing
    # latest.
    #
    # If you parse from link, the status will be set to :aired by default
    def initialize(link, status = nil, parent=nil)
      case link.name
      when 'a'
        @title = link.text
        @url = link['href']
        @status = status ? status : :aired
        @parent = parent
      when 'tr'
        link.css("td").each do | td |
          if td['border'] == "1"
            a = td.css("a")[0]
            @title = a.text
            @url = a['href']
          elsif
            td["width"] == "20%"
            span = td.css("span")
            if span.to_s.include? "video-Subbed"
              @status = :aired
            elsif span.to_s.include? "video-yet"
              @status = :not_aired
            else
              @status = :unknown
            end
          end
        end
        @parent = parent
      else
        @title = "Unknown"
        @url = nil
        @status = :unknown
      end
    end

    # Returns episode title as string
    def title
      return @title
    end

    # Returns status
    # The status can either be :aired, :not-aired, :unknown
    # :unknown is used to filter bad episodes.
    def status
      return @status
    end

    def parent
      return @parent
    end

    # Returns the page url as string.
    # The page URL is where this episode can be found.
    # this link is used by streamURL to get the direct stream link
    def pageURL
      return @url
    end

    # Pinoy::EpisodeClass.streamURL
    # call-seq:
    #   streamURL(void) => string
    #   streamURL(void) => nil
    #
    # = Workings
    # This function finds the episodes random id assigned by
    # pinoyanime.
    #
    # Then loads a http://play.pinoyanime.tv link using that ID.
    # then parses the jwplayer javascript to find the direct
    # link to the video.
    #
    # = Returns
    # The function returns an url if it can find one, or nil if
    # the link is missing.
    # This is not uncommon for older series linked on the site
    def streamURL
      if @streamurl == nil
        @streamurl = PinoyFilter::SiteHandler.GetEpisodeStreamURL(@url)
      end

      return @streamurl
    end # end streamURL

    # Custom Inspect Message
    def inspect
      return "#{self}, #{@title}"
    end
  end # end class
end # end module
