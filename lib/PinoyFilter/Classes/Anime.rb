module PinoyFilter

  # The anime class holds @name, @url and @id.
  # It also let you retrieve a list of episodes that are available.
  class Anime

    # To keep count of animes and allow one to
    # refer to them easier, we keep a list of them
    @@count = 0

    # @overload initialize(elem)
    #   Initializes the Anime class
    #   @param [Nokogiri::Element] elem A 'a' tag element from Nokogiri
    # @return [void] Regardless of what happends
    def initialize(element)
      @name = element.text
      @url = element['href']
      @@count += 1
      @id = @@count
    end

    # Will check if this anime is valid. Can be used to filter bad animes out.
    # And by bad, I mean invalid. Not Sword Art Online, trollololllollol.
    # This function is being called by the SiteHandler and will require this to
    # return true, for it to be added to the list.
    # This is a bad way of doing it.
    # @return [true] If the link is valid.
    # @return [false] If it triggers any of the criterias
    #
    # == Triggers
    # * If the link does not go to "pinoyanime.tv". I know this currently breaks
    #   the entire concept of being able to use other links. But until I pass the Site class into this
    #   its going to be broken for any custom sites.
    #
    def valid?
      begin
        if @url.include? "pinoyanime.tv" or @url.include? "theanime.tv"
          return true
        else
          return false
        end
      rescue
        return false
      end
    end

    # The name of the anime
    # @return [String] name
    def name
      return @name
    end

    # Returns anime URL as string
    # @return [String] url
    def url
      return @url
    end

    # Returns the self assigned ID. Based on @@count
    # @return [int] Self assigned ID
    def id
      return @id
    end

    # Used to inspect the elements in IRB.
    # The output has not been prettified for use in production.
    #
    # Hides URL and episodes which would've been present on standard inspect.
    # @return [String] Output string
    def inspect
      return "#{self}, #{@id}, #{@name}"
    end

    # @return [PinoyFilter::Episode[]] Returns an array of episodes.
    def episodes
      if !@episodes
        @episodes = PinoyFilter::SiteHandler.GetEpisodes(@url, nil, self)
      end

      return @episodes
    end # end episodes

  end
end
