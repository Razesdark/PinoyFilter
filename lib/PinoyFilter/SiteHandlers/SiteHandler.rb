# This module stores all classes and models used
# to interact with the PinoyAnime site
module PinoyFilter

  # This class was supposed to hold all the functions that called the server
  # but some of the functions have been pushed to the anime and episode class
  # They should be moved here at some point.
  class SiteHandler
    @html = nil

    def self.html
      @html
    end

    def self.fetch(url)
      @html = Nokogiri::HTML(open(url))
      @html
    end
    # self.GetAnimeList
    #
    # @overload self.GetAnimeList(url)
    #   Returns a list of animes based on the URL.
    #   If this function has been called previously then a cached result will
    #   be returned.
    #   @param [String] url URL that points to the PinoyAnime list of animes.
    # @overload self.GetAnimeList(url, force)
    #   Force can be set to anything but false, to force a uncached version of
    #   the list.
    #   @param [String] url URL that points to the PinoyAnime list of animes.
    #   @param [bool] force
    # = Workings
    # This function calls http://www.pinoyanime.tv/anime-lists/
    # and parses its HTML to get a list of all animation series currently listed on their webpage
    #
    # = Version 0.1.0 specific changes
    # With the new 0.1.0 change, some slight alterations has been done to this function.
    # To enable the GetLatest to link the latest episodes to specific animes and not store them
    # several times in memory, the anime list will now be cached.
    #
    # If your app needs fresh data, you can pass a second argument
    # and set it to true to force the data to be refetched from the website.
    # I don't know how ruby handles memory in the event that data changes.
    # Will it keep the old array or just those elements. Anyhow, to be sure this has become 0.1.0 instead of 0.0.5
    #
    # = Missing functions
    # The current filter is somewhat lacking and extracts to many links to pages
    # that are not formatted correctly and will break the system if you try to "watch" them.
    #
    # In the future it will be improved to do the following
    # * <b>Check if the name is already in the system.</b> The sidebar uses the same html scheme as the main list
    # so current series will be parsed and listed twice with two ID's.
    # Since the link is the same both entities will work, but are useless. Also they do not appear in order.
    def self.GetAnimeList(url, force = false)
      if !@animes || force
        site_html = PinoyFilter::SiteHandler.fetch("#{url}/anime-lists/")
        links = site_html.css("li > a")
        animes = []
        links.each do |link|
          anime = PinoyFilter::Anime.new(link)
          if anime.valid?
            animes << anime
          end
        end

        @animes = animes
      end
      return @animes
    end
    # self.GetEpisodeStreamURL
    #
    # call-seq:
    #   GetEpisodeStreamURL(http_url)
    # = Workings
    # This function will first go to the normal video page
    # and then find the iframe that links to the jwplayer (http://play.pinoyanime.tv)
    # then load that iframe and parse the javascript that holds the video URL.
    def self.GetEpisodeStreamURL(url)
      # Fetch HTML and locate all iframes
      first_page = PinoyFilter::SiteHandler.fetch(url)
      frames = first_page.css("iframe")

      play_link = nil
      frames.each do |frame|
        if frame["title"] == "Pinoy Anime TV"
          play_link = frame["src"]
        end
      end

      if play_link == nil
        urls = URI.extract(first_page.to_s)
        urls.each do |url|
          play_link = url if url.include? "play." and play_link == nil
        end
      end

      # If we could find a link to a http://play.xxx site
      # then continue, else return nil
      video_link = nil
      if play_link
        puts "Fetching sublink from #{play_link}"
        # Fetch Play HTML and parse HTML
        play_site = PinoyFilter::SiteHandler.fetch(play_link)
        params = URI.extract(play_site.to_s)
        #puts "PC: #{play_site}"
        #puts "#{params}"
        params.each do |param|
          if param[param.length-4,4] == ".mp4"
            return param
          end
          if param[param.length-6,4] == ".mp4"
            result = param[0, param.length-2]
            return result
          end
        end
      else
        puts "Couldn't find valid link for that episode"
        return nil
      end
      return video_link
    end

    # @overload self.GetEpisodes(url)
    #   Basic Version of GetEpisodes.
    #   @param [String] url A url to an Anime profile.
    #   @return [PinoyFilter::Episode[]] A list of all episodes pertaining to the anime that called this function.
    #   @return [[]] If no animes were found
    #
    # @overload self.GetEpisodes(url, parent)
    #   Same function with an optional argument to add a parent element.
    #   This is to give access to the PinoyFilter::Anime element that "owns"
    #   the episode
    #   @param [PinoyFilter::Anime] parent The "owner" of the episode
    #   @return [PinoyFilter::Episode[]] A list of all episodes pertaining to the anime that called this function.
    #   @return [[]] If no animes were found
    #
    # = Workings
    # Accepts any http://pinoyanime.tv/anime-name type URL. And will find
    # all the episodes listed on that page and list them in an array.
    #
    # Array is reversed on return because newest episode is on top on their
    # website.
    def self.GetEpisodes(url, status = nil, parent = nil)
      site_data = PinoyFilter::SiteHandler.fetch(url)
      links = site_data.css("table.table").css("tbody").css("tr")

      if links.count == 0
        raise StandardError("No links found in file")
      end
      episodes = []
      links.each do |link|
        episode = PinoyFilter::Episode.new(link, nil, parent)
        if(episode.status == :unknown)

        else
          episodes << episode
        end
      end

      return episodes.reverse
    end

    # @overload self.GetLatest(url)
    #   @param [String] url URL to the front page
    #   @return [PinoyFilter::Episode[]] - A list of all animes. With .parent active
    #
    # = Workings
    # Loads frontpage and gives an array of the latest updated animes.
    # For now, the titles will be kinda shitty. And it doesn't store the name of the anime.
    # So for any views, you should use the pageURL attribute.
    def self.GetLatest(url)
      begin
        PinoyFilter::SiteHandler.GetAnimeList(url)

        site_data = PinoyFilter::SiteHandler.fetch(url)
        episodes = site_data.css(".episode")

        stuff = []
        episodes.each do |ep|
          d = ep.css(".episode-details").css("a")[0]['href']
          anime = nil
          @animes.each do |ani|
            if d == ani.url
              anime = ani
            end
          end
          stuff << PinoyFilter::Episode.new(ep.css(".episode-eps").css("a")[0], nil, anime)
        end
      end

      return stuff
    end
  end

end
