# PinoyFilter
PinoyFilter is ment to wrap around PinoyAnime.tv and extract data from that site
including things such as.
  * List of Animes
    * Their episodes
      * The direct stream URL

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'PinoyFilter'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install PinoyFilter

## Usage

PinoyFilter is a simple wrapper for pinoyanime.tv.
By creating a new PinoyFilter::Site element, you can use the function element.anime_list to get a list of animes.

Each of the returned objects is a PinoyFilter::Anime class, which holds a list of episodes available using animeElement.episode_list

By default the current functions will work
```ruby
@site = PinoyFilter::Site.new # if you want to use default pinoyanime link, arguments are not required.

# To get a list of animes
animes = @site.animes

# Episodes can be referenced from the anime class
episodes = animes[x].episodes
```

## Contributing

1. Fork it ( https://github.com/Razesdark/PinoyFilter/fork )
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request
