= Version 0.1.0
* Added inspect function for episodes
* Added a parent element for episodes, so you can reference the name of the series
*

from the episode.
= Version 0.0.4
* Added Anime.valid? to filter out bad links in the anime list
  * To keep ID's consistent, the filtered IDs will not be reused.
* Bugfixes
* Anime ID's are now listed correctly

= Version 0.0.3
* Added latest support

= Version 0.0.2
*	The first unstable release
* Can find and return correct data. But doesn't gracefully fail if the data is unexpected.
