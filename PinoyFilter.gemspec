# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'PinoyFilter/version'

Gem::Specification.new do |spec|
  spec.name          = "PinoyFilter"
  spec.version       = PinoyFilter::VERSION
  spec.authors       = ["Tommy Stigen Olsen"]
  spec.email         = ["tommysolsen@gmail.com"]
  spec.summary       = "Wrapper to interface with PinoyAnime.tv"
  spec.description   = %q{Write a longer description. Optional.}
  spec.homepage      = ""
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_runtime_dependency "nokogiri", "~> 1.6.3"
  spec.add_runtime_dependency "json"
  spec.add_development_dependency "bundler", "~> 1.7"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec"
end
