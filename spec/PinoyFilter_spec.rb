require 'spec_helper'

describe PinoyFilter do
  it 'has a version number' do
    expect(PinoyFilter::VERSION).not_to be nil
  end

  describe '#new' do
    it 'stores url' do
      obj = PinoyFilter::Site.new "http://test.no/"
      expect(obj.url).to eq "http://test.no/"
    end
    it 'appends http:// if missing' do
      obj = PinoyFilter::Site.new "test.no/"
      expect(obj.url).to eq "http://test.no/"
    end
    it 'appends / at end of url if needed' do
      expect(PinoyFilter::Site.new("http://test.no").url).to eq "http://test.no/"
    end
  end

  describe '#url' do
    it 'returns url' do
      expect(PinoyFilter::Site.new("http://animesomething.com/").url).to eq "http://animesomething.com/"
    end
  end

  describe '#animes' do
    before(:all) do
      @obj = PinoyFilter::Site.new
    end
    it 'returns list' do
      expect(@obj.animes.class).to eq Array
    end
    it 'includes only animes' do
      @obj.animes.each do |anime|
        expect(anime.class).to eq PinoyFilter::Anime
      end
    end
  end

  describe "#latest" do
    before(:all) do
      @obj = PinoyFilter::Site.new
    end
    it 'returns list' do
      expect(@obj.animes.class).to eq Array
    end
    it 'includes only animes' do
      @obj.animes.each do |anime|
        expect(anime.class).to eq PinoyFilter::Anime
      end
    end
  end

end
