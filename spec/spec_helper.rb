$LOAD_PATH.unshift File.expand_path('../../lib', __FILE__)
require 'PinoyFilter'
require "nokogiri"
require "open-uri"
require "json"
require "uri"

class FakeNokogiri
    attr_reader :keys
    def initialize
        @keys = {}
    end
    def []=(key, value)
        @keys[key] = value
    end
    def [](key)
        return @keys[key]
    end

    def method_missing(name, *arguments)
        if name[-1] == "="
            @keys[name[0..-2]] = arguments[0]
        else
            key = name.to_s
            return @keys[key]
        end
    end
end

RSpec::Matchers.define :be_boolean do
  match do |actual|
    expect(actual).to satisfy { |x| x == true || x == false }
  end
end