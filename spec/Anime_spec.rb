require 'spec_helper.rb'

describe PinoyFilter::Anime do
  before(:all) do
      @invalid_element = FakeNokogiri.new
      @invalid_element.text = "Nokogiri"
      @invalid_element['href'] = "http://www.test.no/anime"

      @valid_element = FakeNokogiri.new
      @valid_element.text = "Nokogiri2"
      @valid_element['href'] = 'http://www.theanime.tv/log-horizon/'

      @invalid_anime = PinoyFilter::Anime.new(@invalid_element)
      @valid_anime = PinoyFilter::Anime.new(@valid_element)
    end
  describe '#new' do
    it 'creates a new anime' do
      expect(@invalid_anime.class).to eq PinoyFilter::Anime
      expect(@valid_anime.class).to eq PinoyFilter::Anime
    end 
    describe "from nokogiri element" do
      it 'retrieves correct name' do
        expect(@invalid_anime.name).to eq "Nokogiri"
        expect(@valid_anime.name).to eq "Nokogiri2"
      end
      it 'retrieves correct url' do
        expect(@invalid_anime.url).to eq "http://www.test.no/anime"
        expect(@valid_anime.url).to eq 'http://www.theanime.tv/log-horizon/'
      end
    end
    it 'assigns inique ids' do
      expect(@valid_anime.id).to eq @invalid_anime.id+1
    end
  end
  describe '#valid' do
    it 'returns boolean' do
      expect(@invalid_anime.valid?).to be_boolean
      expect(@valid_anime.valid?).to be_boolean
    end
    it 'returns false when url doesn\'t point to PA domains' do
      expect(@invalid_anime.valid?).to eq false
      expect(@valid_anime.valid?).to eq true
    end
    it 'returns true when url points to an PA domain' do
      expect(@invalid_anime.valid?).to eq false
      expect(@valid_anime.valid?).to eq true
    end
  end
  describe '#name' do
    it 'returns string' do
      expect(@invalid_anime.name).to be_kind_of String
      expect(@valid_anime.name).to be_kind_of String
    end
    it 'stores correct values' do
      expect(@invalid_anime.name).to eq "Nokogiri"
      expect(@valid_anime.name).to eq "Nokogiri2"
    end
  end
  describe '#url' do
    it 'returns string' do
      expect(@invalid_anime.url).to be_kind_of String
      expect(@valid_anime.url).to be_kind_of String
    end
    it 'stores correct values' do
      expect(@invalid_anime.url).to eq "http://www.test.no/anime"
      expect(@valid_anime.url).to eq 'http://www.theanime.tv/log-horizon/'
    end
  end
  describe '#episodes' do
    it 'returns list' do
      expect(@valid_anime.episodes).to be_kind_of Array
    end
    it 'returns only episode elements' do
      @valid_anime.episodes.each do |ep|
        expect(ep).to be_kind_of PinoyFilter::Episode
      end
    end
  end
end